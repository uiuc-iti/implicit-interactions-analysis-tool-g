import { Express } from "express";
import session from "express-session";
import cookieParser from "cookie-parser";



function configureSession(app: Express) {
    app.use(cookieParser(process.env.COOKIE_SECRET));

    var secretCook = process.env.COOKIE_SECRET;
    if(!secretCook)
        secretCook = "SECRETE_NOT_DEFINED_IN_ENV";

    app.use(session({
            resave: false,
            saveUninitialized: false,
            secret: secretCook,
        }));

}

export default configureSession;