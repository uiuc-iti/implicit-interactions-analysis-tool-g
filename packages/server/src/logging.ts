import { Express } from "express";
import morgan from 'morgan';
import fs from 'fs';

function configureLogging(app: Express) {

    switch(app.get('env')) {
        case 'development':
            app.use(morgan('dev'));
            break;
        case 'production':
            const stream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});
            app.use(morgan('combined', {stream}));
    }

}

export default configureLogging;