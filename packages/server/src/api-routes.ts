import express from "express";

const apiRoot = "/api/";

function configureAPIRouting(router: express.Router) {

    router.get(apiRoot + "user/:id")
    router.post(apiRoot + "user", (req, res, next) => { // Create user

    });

    router.delete(apiRoot + "user", (req, res, next) => { // Delete user

    });

    router.patch(apiRoot + "user/:id", (req, res, next) => { // Update user

    });

    
};


export default configureAPIRouting;