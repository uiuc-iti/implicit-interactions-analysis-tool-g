import express, { Express } from "express";
import configureAPIRouting from "./api-routes";

function configureRouting(app : Express) {
    const router = express.Router();
    app.use(router);

    router.get("/", (req, res, next) => {
        res.render('index');
    });
    
    configureAPIRouting(app);
}


export default configureRouting;