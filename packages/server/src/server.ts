import express from "express";
import configureLogging from './logging';
import configureSession from './session';
import configureRouting from './routes';
import configureErrorHandling from "./error-handling";
import configureHandlebars from "./handlebars";

require("dotenv").config();

const app = express();

configureHandlebars(app);
    
configureLogging(app);

configureSession(app);
    
configureRouting(app);

configureErrorHandling(app);
    
if(require.main === module) {
    // Application was run directly, so start the server.
    startServer(parseInt(process.env.SERVER_PORT || "3000"));
}

function startServer(port: number) {
    app.listen({ port: port }, () => {
        console.log(`Express started in ${app.get('env')} mode on port ${port}`);
    });
}

export default startServer;
