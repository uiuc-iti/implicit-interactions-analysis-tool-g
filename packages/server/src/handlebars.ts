import { Express } from "express";
import { engine as expressHandlebars }  from 'express-handlebars';

function configureHandlebars(app : Express) {
    app.engine('handlebars', expressHandlebars({
        defaultLayout: 'main'
    }));
    app.set('view engine', 'handlebars');
};


export default configureHandlebars;