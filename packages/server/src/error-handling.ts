import express, { Express, NextFunction } from "express";

function configureErrorHandling(app : Express) {
    process.on('uncaughtException', err => {
        console.error('UNCAUGHT EXCEPTION\n', err.stack);
        process.exit(1);
    });

    app.use((err: Error, req: express.Request, res: express.Response, next: NextFunction) => {
        console.error(err.message, err.stack);
        res.status(500).render('500');
    });
    
};


export default configureErrorHandling;