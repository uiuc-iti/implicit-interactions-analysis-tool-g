import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import * as monaco from 'monaco-editor';
import { CommentsDisabled } from '@mui/icons-material';

monaco.languages.register({
  id: 'c2ka',
});


monaco.languages.setLanguageConfiguration('c2ka', {
  autoClosingPairs: [
    {
      open: 'where ', close: '\nend'
    },
    {
      open: "where\n", close: 'end'
    },
    {
      open: '(', close: ')'
    },
    {
      open: '[', close: ']'
    }
  ],

  comments: {
    lineComment: '//',
    blockComment: ['/', '*']
  },

  folding: {
    offSide: true,
    markers: {
      start: /^(where).*/,
      end: /^(end).*/
    }
  }
});


monaco.languages.setMonarchTokensProvider('c2ka', {
  operators: ['=', '=>', ':=', '+', '->', '&&', '~', '|'],
  symbols: /[=><!~?:&|+\-*\/\^%]+/,
  keywords: ['begin', 'end', 'where'],
  tokenizer: {
    root: [
      [/@?[a-zA-Z][\w$]*/, {
        cases: {
          '@keywords': 'keyword',
          '@default': 'variable',
        }
      }],
      [/".*?"/, 'string'],
      [/.*^(\/\/).*/, 'comment'],
      [/@symbols/, { cases: { '@operators': 'operator',
                              '@default'  : '' } } ],
    ]
  }
});


function createDependencyProposals(range: { startLineNumber: number; endLineNumber: number; startColumn: number; endColumn: number; }) {
  return [
      {
          label: "begin",
          kind: monaco.languages.CompletionItemKind.Keyword,
          insertText: "begin",
          range: range
      },
      {
          label: "where",
          kind: monaco.languages.CompletionItemKind.Keyword,
          insertText: "where",
          range: range
      },
      {
          label: "end",
          kind: monaco.languages.CompletionItemKind.Keyword,
          insertText: "end",
          range: range
      },
      {
          label: '"def"',
          kind: monaco.languages.CompletionItemKind.Function,
          documentation: "Define behavior, stimulus, agent using begin, where, and end",
          insertText: "begin NAME where\n\tDEFINITION\nend",
          range: range
      },

      // Add more keywords here if needed
  ];
}


monaco.languages.registerCompletionItemProvider('c2ka', {
  provideCompletionItems: function(model, position) {
      var range = {
          startLineNumber: position.lineNumber,
          endLineNumber: position.lineNumber,
          startColumn: (position.column-1),
          endColumn: position.column,
      };
      return {
          suggestions: createDependencyProposals(range)
      };
  }
});


monaco.editor.defineTheme('c2ka-theme', {
  base: 'vs',
  rules: [
    { token: 'keyword', foreground: '#FF6600', fontStyle: 'bold' },
    { token: 'comment', foreground: '#7FB785' },
    { token: 'string', foreground: '#009966' },
    { token: 'variable', foreground: '#006699'},
  ],
  inherit: false,
  colors: {}
});


ReactDOM.render(
  <React.StrictMode>
    {/* Get icons required for display */}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"></link>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
