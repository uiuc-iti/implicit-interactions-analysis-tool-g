import AnalysisResult from "./AnalysisResult";
import WorkspaceComponent from "./WorkspaceComponent";

export default class SystemDesign extends WorkspaceComponent {
    public getAllowedChildClasses(): (typeof WorkspaceComponent)[] {
        return [AnalysisResult];
    }

}