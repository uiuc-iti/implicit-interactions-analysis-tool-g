import TreeIcon from "./TreeIcon";

export default abstract class WorkspaceComponent {
    protected _parent?: WorkspaceComponent = undefined;
    protected _children: WorkspaceComponent[] = [];
    protected _name: string = "";
    protected _altText: string = "";
    protected _icon: TreeIcon = TreeIcon.None;

    protected setParent(parent: WorkspaceComponent) {
        parent.addChild(this);
    }

    protected parent() : WorkspaceComponent | undefined {
        return this._parent;
    }

    public addChild(child: WorkspaceComponent) : boolean {
        if(!this._children.includes(child) && this.canHaveChild(child)) {
            child._parent?.removeChild(child);
            this._children.push(child);
            child._parent = this;
            return true;
        }
        return false;
    }

    protected removeChild(child: WorkspaceComponent) : boolean {
        if(this._children.includes(child)) {
            this._children = this._children.filter(c => c !== child);
            child._parent = undefined;
            return true;
        }
        return false;
    }

    public abstract getAllowedChildClasses() : Array<typeof WorkspaceComponent>;

    protected canHaveChild(child: WorkspaceComponent) : boolean {
        const allowedTypes = this.getAllowedChildClasses();

        return allowedTypes.some(element => {
            return (child instanceof element)
        });
    }

    get name() : string {
        return this._name;
    }

    set name(newName : string) {
        this._name = newName;
    }

    get altText() : string {
        return this._altText;
    }

    set altText(newAltText : string) {
        this._altText = newAltText;
    }

    get icon() : TreeIcon {
        return this._icon;
    }

    set icon(newIcon : TreeIcon) {
        this._icon = newIcon;
    }

    public hasChildren() : boolean {
        return this._children.length > 0;
    }

    get children() : WorkspaceComponent[] {
        return [...this._children];
    }
}