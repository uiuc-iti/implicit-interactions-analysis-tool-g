import AnalysisResult from "./AnalysisResult";
import Project from "./Project";
import RootWorkspaceComponent from "./RootWorkspaceComponent";
import SystemDesign from "./SystemDesign";
import WorkspaceComponent from "./WorkspaceComponent";


export default class Workspace {
    private static instance: Workspace;

    protected root: WorkspaceComponent = new RootWorkspaceComponent();

    private constructor() {
        // Load fake data temporarily
        let p : Project = new Project();
        p.name = "Chemical Reactor";
        this.root.addChild(p);

        p = new Project();
        p.name = "Manufacturing Cell";
        this.root.addChild(p);

        let sd : SystemDesign = new SystemDesign();
        sd.name = "Failed Press";
        p.addChild(sd);

        sd = new SystemDesign();
        sd.name = "Rapid Variation";
        p.addChild(sd);

        let ar : AnalysisResult = new AnalysisResult();
        ar.name = "[11-11-2021 9:00]";
        sd.addChild(ar);

        ar = new AnalysisResult();
        ar.name = "[11-11-2021 8:42]";
        sd.addChild(ar);

        sd = new SystemDesign();
        sd.name = "Single Line";
        p.addChild(sd);

        p = new Project();
        p.name = "Port Terminal";
        this.root.addChild(p);
        // End load fake data temporarily

    }

    public static getInstance() : Workspace {
        if(!Workspace.instance)
            Workspace.instance = new Workspace();

        return Workspace.instance;
    }

    get rootItem() : WorkspaceComponent {
        return this.root;
    }


}