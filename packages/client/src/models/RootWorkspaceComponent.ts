import WorkspaceComponent from "./WorkspaceComponent";
import Project from "./Project";

export default class RootWorkspaceComponent extends WorkspaceComponent {
    public getAllowedChildClasses(): (typeof WorkspaceComponent)[] {
        return [Project];
    }


    
}