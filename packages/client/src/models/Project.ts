import SystemDesign from "./SystemDesign";
import WorkspaceComponent from "./WorkspaceComponent";

export default class Project extends WorkspaceComponent {
    public getAllowedChildClasses(): (typeof WorkspaceComponent)[] {
        return [SystemDesign];
    }


    
}