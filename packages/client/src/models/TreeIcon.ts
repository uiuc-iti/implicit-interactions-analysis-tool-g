
export default class TreeIcon {
    public static None = new TreeIcon("None");
    public static Folder = new TreeIcon("Folder");
    public static TextFile = new TreeIcon("TextFile");
    public static DetailsFile = new TreeIcon("DetailsFile");


    private label : string;

    private constructor(label: string) {
        this.label = label;
    }

    public getLabel() {
        return this.label;
    }
}