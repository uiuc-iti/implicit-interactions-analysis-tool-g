import './App.css';
import WorkspaceTree from './components/workspaceTree/WorkspaceTree';
import MainEditor from './components/mainEditor/MainEditor';
import EditorTabs from './components/tabsArea/EditorTabs';
import FooterArea from './components/FooterArea';
import HeaderArea from './components/header/HeaderArea';

function App() {
  return (
    <div className="App">
      <WorkspaceTree />
      <HeaderArea />
      <MainEditor />
      <EditorTabs />
      <FooterArea />
      
    </div>
  );
}


export default App;
