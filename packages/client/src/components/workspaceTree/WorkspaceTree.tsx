import WorkspaceComponent from '../../models/WorkspaceComponent';
import WorkspaceTreeDataProvider from './WorkspaceTreeDataProvider';
import "./WorkspaceTree.css"
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';


const WorkspaceTree = () => {
  // const items = {
  //     root: {
  //       index: 'root',
  //       hasChildren: true,
  //       children: ['child1', 'child2', 'child8'],
  //       data: 'Root item',
  //     },
  //     child1: {
  //       index: 'child1',
  //       children: [],
  //       // data: 'Child item 1',
  //       data: 'ChemicalReactor',
  //     },
  //     child2: {
  //       index: 'child2',
  //       hasChildren: true,
  //       children: ['child3', 'child4', 'child7'],
  //       // data: 'Child item 2',
  //       data: 'Manufacturing Cell'
  //     },
  //     child3: {
  //       index: 'child3',
  //       children: [],
  //       data: 'FailedPress',
  //     },
  //     child4: {
  //       index: 'child4',
  //       hasChildren: true,
  //       children: ['child5', 'child6'],
  //       data: 'Rapid Variation',
  //     },
  //     child5: {
  //       index: 'child5',
  //       children: [],
  //       data: '[11-11-2021 9:00]',
  //     },
  //     child6: {
  //       index: 'child6',
  //       children: [],
  //       data: '[11-11-2021 8:42]',
  //     },
  //     child7: {
  //       index: 'child7',
  //       children: [],
  //       data: 'SingleLine',
  //     },
  //     child8: {
  //       index: 'child8',
  //       children: [],
  //       data: 'Port Terminal',
  //     },
  //   };
  
  //   return <div className="workspaceTree">
  //     <UncontrolledTreeEnvironment
  //     dataProvider={new WorkspaceTreeDataProvider()}
  //     getItemTitle={(item : TreeItem<WorkspaceComponent>) => item.data.name}
  //     viewState={{}}
  //   >
  //     <Tree treeId="workspaceTree" rootItem="0" treeLabel="Project Workspace" />
  //   </UncontrolledTreeEnvironment>
  //   <UncontrolledTreeEnvironment
  //   dataProvider={new StaticTreeDataProvider(items, (item, data) => ({ ...item, data }))}
  //   getItemTitle={item => item.data}
  //   // Add icons to workspace tree
  //   renderItemTitle={({ title }) => <span><span className = "material-icons md-12">folder</span>&nbsp;{title}</span>}
  //   viewState={{}}
  // >
  //   <Tree treeId="tree-1" rootItem="root" treeLabel="Tree Example" />
  // </UncontrolledTreeEnvironment>
  // </div>;

    return <div className='workspaceTree'>
      <TreeView
        aria-label="file system navigator"
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        sx={{ height: 240, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
      >
        <TreeItem nodeId="1" label="Applications">
          <TreeItem nodeId="2" label="Calendar" />
        </TreeItem>
        <TreeItem nodeId="5" label="Documents">
          <TreeItem nodeId="10" label="OSS" />
          <TreeItem nodeId="6" label="MUI">
            <TreeItem nodeId="8" label="index.js" />
          </TreeItem>
        </TreeItem>
      </TreeView>
    </div>;
}

export default WorkspaceTree;