import WorkspaceTreeItem from "./WorkspaceTreeItem";


export default class WorkspaceTreeDataProvider {

    public getTreeItem(itemId: number) : Promise<any> {
        return new Promise(res => {
            let item = WorkspaceTreeItem.GetItemWithIndex(itemId);
            if(item !== undefined)
                res(item);
        });
    }

    public getTreeItems(itemIds : number[]) : Promise<any[]> {
        return new Promise(res => {
            let items : WorkspaceTreeItem[] = [];
            itemIds.forEach((idx) => {
                let item = WorkspaceTreeItem.GetItemWithIndex(idx);
                if(item !== undefined)
                    items.push(item);
            });
            res(items);
        })
    }
    //getTreeItems?: ((itemIds: TreeItemIndex[]) => Promise<TreeItem<any>[]>) | undefined;
    //onRenameItem?: ((item: TreeItem<any>, name: string) => Promise<void>) | undefined;
    //onChangeItemChildren?: ((itemId: TreeItemIndex, newChildren: TreeItemIndex[]) => Promise<void>) | undefined;
    
}