import WorkspaceComponent from "../../models/WorkspaceComponent";
import Workspace from "../../models/Workspace";

export default class WorkspaceTreeItem {

    private static rootItem : WorkspaceTreeItem;
    private static nextId : number;
    private static idToItem : WorkspaceTreeItem[];
    private static idToComponent : WorkspaceComponent[];
    public static getRootTreeItem() {
        return this.rootItem;
    }
    public static GetItemWithIndex(id : number) {
        if(typeof(id) === 'number') {
            return this.idToItem[id];
        } else return undefined;
    }

    static initialize() {
        WorkspaceTreeItem.nextId = 0;
        WorkspaceTreeItem.idToItem = [];
        WorkspaceTreeItem.idToComponent = [];
        WorkspaceTreeItem.rootItem = new WorkspaceTreeItem(Workspace.getInstance().rootItem);
    }


    private _index : number;
    private _data : WorkspaceComponent;

    public constructor(wc : WorkspaceComponent) {
        this._index = WorkspaceTreeItem.nextId++;
        this._data = wc;
        WorkspaceTreeItem.idToItem[this._index] = this;
        WorkspaceTreeItem.idToComponent[this._index] = this._data;
    }

    get index(): number {
        return this._index;
    }
    get children() : number[] {
        let childIdxs : number[] = []

        this._data.children.forEach ((child) => {
            let idx = WorkspaceTreeItem.idToComponent.indexOf(child);
            if(idx === -1 ) {
                let newItem = new WorkspaceTreeItem(child);
                idx = newItem._index;
            }
            childIdxs.push(idx);
        });

        return childIdxs;

    }
    get hasChildren() : boolean {
        return this._data.hasChildren();
    }

    get canMove() : boolean {
        return false;
    }

    get canRename() : boolean {
        return false;
    }
    get data() : WorkspaceComponent {
        return this._data;
    }

}

WorkspaceTreeItem.initialize();