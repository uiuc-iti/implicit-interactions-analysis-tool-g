
const FooterArea = () => {
    return <footer className="footer">&copy; 2022 - MIT License</footer>;
}

export default FooterArea;