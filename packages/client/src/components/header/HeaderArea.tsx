import BreadCrumb from "./BreadCrumb";
import TitleArea from "./TitleArea";

const HeaderArea = () => {
    return <div className="headerArea">
        <TitleArea />
        <BreadCrumb />
    </div>
}

export default HeaderArea;