
const BreadCrumb = () => {
    return <div className="breadCrumb">
        <p>
            <span className = "material-icons md-12">folder</span>&nbsp; Manufacturing Cell &gt; &nbsp;
            <span className = "material-icons md-12">description</span>&nbsp; Rapid Variation (modified)
        </p>
    </div>;
}

export default BreadCrumb;