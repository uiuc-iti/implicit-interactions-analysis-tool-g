import UserArea from "./UserArea";

const TitleArea = () => {
    return <div className="titleArea">
        <h1>Implicit Interactions Analysis Tool</h1>
        <UserArea />
    </div>;
}

export default TitleArea;