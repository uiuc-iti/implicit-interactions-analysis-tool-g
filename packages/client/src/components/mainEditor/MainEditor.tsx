import MonacoEditor from '@uiw/react-monacoeditor';
import "./MainEditor.css"

const MainEditor = () => {
    return <main className="mainContent">
        <MonacoEditor
  language="c2ka"
  value=''
  options={{
    theme: 'c2ka-theme',
    value: "//Write or paste code here...", // Keywords: begin, end, where
    tabSize: 8,
    mouseWheelZoom: true,
    smoothScrolling: true,
    dragAndDrop: true,
    
    // To fold the 'end' part, indent it over 1 or more tabs from the 'begin'
  }}
/>
    </main>;
}

export default MainEditor;