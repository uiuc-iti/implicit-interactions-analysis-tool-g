import React from "react";

const onClickAnalyze = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
) => {
    e.preventDefault();
    
};

const onAddAgent = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
}

const EditorTabs = () => {
    return <div className="belowEditor">
        <button className="addAgent" onClick={onAddAgent}>+</button>
        <ul className="tabs">
            <li><button className="agents">C</button></li>
            <li><button className="agents">H</button></li>
            <li><button className="agents">P</button></li>
            <li><button className="agents">S</button></li>
        </ul>
        <button className="analyzeBut" onClick={onClickAnalyze}>Analyze</button>
    </div>;
}

export default EditorTabs;