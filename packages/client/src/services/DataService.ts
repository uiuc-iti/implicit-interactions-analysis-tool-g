import Project from "../models/Project";
import WorkspaceComponent from "../models/WorkspaceComponent";

export async function getRootElements(): Promise<Array<WorkspaceComponent>> {
    const promise = new Promise<Array<WorkspaceComponent>>((res, rej) => {
        const comps = [];
        const proj1 = new Project();
        proj1.name = "Project 1";
        comps.push(proj1);
        const proj2 = new Project();
        proj2.name = "Project 2";
        comps.push(proj2);
        const proj3 = new Project();
        proj3.name = "Project 3";
        comps.push(proj3);

        res(comps);
    });
    return promise;
}