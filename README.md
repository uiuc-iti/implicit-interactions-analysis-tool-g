# Implicit Interactions Analysis Tool - Graphical

This project contains the graphical client for the Implicit Interactions Analysis Tool. This client connects with a MySQL database for data persistence and an instance of the Implicit Interactions Analysis Tool (no G!). 

## Architecture

![IIAT Project Architecture with the graphical client highlighted.](images/IIAT-Architect_IIATG.png "IIAT Project Architecture with the graphical client highlighted.")

## Configuration Options

The following options can be set on the application by setting the associated environment variables on the container running the application.
### Email SMTP Service

### HTTP/HTTPS


### Port

